'use strict';

/**
 * completely recreates the project table by using the data
 * within the project array
 * author: Christopher Hu
 * @param {object} arrayObj, project array
 */
function reconstructProjectTable(arrayObj){
	let headers = document.getElementsByTagName('tr')[0];
	document.getElementsByTagName('table')[0].remove();
	createTagElement('table', document.getElementById('projTable'));
	document.getElementsByTagName('table')[0].appendChild(headers);

	for (let project of arrayObj) {
		createRowFromArrayObjects(project);
	}
}


/**
 * creates a single row passed as an arguement
 * author: Vinh Dat Hoang & Christopher Hu (code refactor)
 * @param {object} project
 */
function createRowFromArrayObjects(project) {
	let table = document.getElementsByTagName('table')[0];


	createTagElement('tr', table);
	let rows = table.getElementsByTagName('tr');
	let tdIndex = 0;
	Object.values(project).forEach(projVal => {
		createTagElement('td', rows[rows.length - 1]);
		rows[rows.length - 1].getElementsByTagName('td')[tdIndex].textContent = projVal;
		tdIndex++;
	});

	createModityButtons(rows);
}

/**
 * createTagElement creates a new tag and appends it as a child to the given parent.
 * author: Christopher Hu
 * @param {string} tagName, contains the string of desire tag such as 'tr' or 'td'
 * @param {object} parent , the element that tagName will append to.
 */
function createTagElement(tagName, parent) {
	let tag = document.createElement(tagName);
	parent.appendChild(tag);
}

/**
 * createModifyButtons creates an element 'td' containing two buttons (edit and delete)
 * then appends it to the newly created row in the projects table.
 * author: Christopher Hu
 * @param {object} rows , contains all 'tr' data within the table.
 */
function createModityButtons(rows) {
	let td = document.createElement('td');

	let edit = document.createElement('input');
	edit.setAttribute('type', 'image');
	edit.setAttribute('src', '../images/Edit_icon.png');
	edit.setAttribute('id', `edit${rows.length - 1}`);
	td.appendChild(edit);

	let del = document.createElement('input');
	del.setAttribute('type', 'image');
	del.setAttribute('src', '../images/delete_icon.png');
	del.setAttribute('id', `delete${rows.length - 1}`);
	td.appendChild(del);


	rows[rows.length - 1].appendChild(td);
	document.getElementById(`delete${rows.length - 1}`).addEventListener('click', confirmDelete);
	document.getElementById(`edit${rows.length - 1}`).addEventListener('click', editSaveHandler);
}


/**
 * removes a project from the projects array and from the table
 * @param {HTMLCollection} event, delete button element situated far right of project row
 * author: Christopher Hu
 */
function removeRow(event) {
	let rmTR = document.getElementById(event.target.id).parentNode.parentNode;
	for (let i = 1; i < document.getElementsByTagName('tr').length; i++) {
		if (document.getElementsByTagName('tr')[i] === rmTR) {
			arrayObj.splice(i - 1, 1);
		}
	}

	// update status bar
	let project = rmTR.getElementsByTagName('td')[0].innerText;
	let statusbar = document.getElementById('filterResultText');
	statusbar.innerText = `${project} have been removed...`;

	// remove row from table
    document.getElementsByTagName('table')[0].removeChild(rmTR);
	
	// verify if arrayObj is empty so that write to local storage button be switched between enable and disable
	let button = document.getElementById('btnWrite');
	enable_disable_WriteLocalButton(button);
}

/**
 * attached event listener to all input elements.
 * attached setElementFeedback() function to input elements.
 * create feedback icon and feedback text.
 * author: Vinh Dat Hoang
 * @param {HTMLCollection} div 
 */
function inputHandler(div) {
	let position = 1;
	
	for (let i = 0; i < div.length - 1; i++) {
		for (let j = 0; j < div[i].childElementCount; j++) {
			let field = div[i].children[j].children[position];
			field.addEventListener('input', setElementFeedback);

			let imageValidation = document.createElement('input');
			imageValidation.setAttribute('style', 'display:none');
			div[i].children[j].appendChild(imageValidation);
			imageValidation.setAttribute('class', 'feedbackIcon');

			let textValidation = document.createElement('span');
			div[i].children[j].appendChild(textValidation);
			textValidation.setAttribute('class', 'feedbackText');
		}
	}
}

/**
 * varify if input element value is valid and match regex pattern.
 * if valid then green check icon is shown and set projectId to true in global variable validateObject.
 * if invalid then x mark icon is shown and span element display a message.
 * author: Vinh Dat Hoang
 * @param {String} current 
 * @param {HTMLCollection} icon --> input element of type image 
 * @param {HTMLCollection} textValidation
 */
function valid(checkValid, key, icon, textValidation) {

	let name = (key == 'projectId'? 'project id': (key == 'ownerName'? 'owner name': key));
	
	if (checkValid == true) {
		icon.setAttribute('src', '../images/valid.png');
		textValidation.setAttribute('style', 'display:none');
		validateObject[key] = true;
	} else {
		icon.setAttribute('src', '../images/wrong.png');
		textValidation.setAttribute('style', 'display:inline');
		textValidation.innerText = `\nWrong format for ${name}...`;
		validateObject[key] = false;
	}
}

/**
 * set element feedback with green check icon, red x mark icon and text box information for the user. 
 * author: Vinh Dat Hoang
 * @param {HTMLCollection} event --> input element currently receiving new value
 */
function setElementFeedback(event) {
	let div = document.getElementById(event.target.id).parentNode;
	let icon = div.children[div.children.length - 2];
	let textValidation = div.children[div.children.length - 1];
	icon.setAttribute('type', 'image');
	icon.setAttribute('style', 'display:inline');

	// verify which field is current target and varify if input element value is valid or invalid
	let current = this.value;

	for(let key in patternArrObj){
		if (this.id == key) {
			let checkValid = false; 
			if (current.search(patternArrObj[key]) > -1) {
				
				checkValid = true;
				valid(checkValid, key, icon, textValidation);

			}else{

				valid(checkValid, key, icon, textValidation);
			}
		}
	}

	// passed description as valid
	validateObject.description = true;

	// verify if add button should be enable or disable 
	let button = document.getElementById('btnAdd');
	enable_disable_Button(button);
}

/**
 * disable add button and write to local storage button.
 * use when page load for the first time.
 * author: Vinh Dat Hoang
 * @param {HTMLCollection} button 
 */
function disable_Button(button) {
	
	button.disabled = true;
	button.style.backgroundColor = 'red';

}

/**
 * verify if write to local button is enable or disable.
 * when arrayObj is empty,  write to local buttonn is disable and turn red. 
 * if arrayObj is not empty then write to local button is enable and turn green. 
 * author: Vinh Dat Hoang
 * @param {HTMLCollection} button 
 */
function enable_disable_WriteLocalButton(button) {

	if (arrayObj.length == 0) {
		button.disabled = true;
		button.style.backgroundColor = 'red';
	} else {
		button.disabled = false;
		button.style.backgroundColor = 'rgb(64, 179, 64)';
	}
}

/**
 * verify if add button is enable or disable.
 * when all keys value of validateObject is true, add button is enable and turn green. 
 * if one or more keys value of validateObject is false then add button is disable and turn red.
 * author: Vinh Dat Hoang
 * @param {HTMLCollection} button 
 */
function enable_disable_Button(button) {

	let passedTest = false;
	for (let key in validateObject) {
		if (validateObject[key] == true) {
			passedTest = true;
		} else {
			passedTest = false;
			break;
		}
	}
	if (passedTest == true) {
		button.disabled = false;
		button.style.backgroundColor = 'rgb(64, 179, 64)';
	} else {
		button.disabled = true;
		button.style.backgroundColor = 'red';
	}
}


/**
 * compares string values of a and b to sort
 * object array alphabetically.
 * author: Christopher Hu
 * @param {object} a, is an object from the projects array 
 * @param {object} b, is an object from the projects array 
 * @returns number
 */
function sortProjectsByString(a,b){
	let key = document.getElementById('sort').value;
	if(a[key] < b[key]){
		return -1;
	}
	else if(a[key] > b[key]){
		return 1;
	}
	else {
		return 0;
	}
}
/**
 * deducts a and b then returns a number.
 * if returned number is negative, b will ascend the sort order.
 * author: Christopher Hu
 * @param {object} a, is an object from the projects array
 * @param {object} b, is an object from the projects array 
 * @returns number
 */
function sortProjectByNumber(a,b){
	let key = document.getElementById('sort').value;
	return a[key] - b[key];
}