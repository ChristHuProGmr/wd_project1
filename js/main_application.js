'use strict';

document.addEventListener("DOMContentLoaded", handlers);


let arrayObj = []; //contains project objects
let sortedObjectArray = [];

let enableSave = 0; //keeps track of toggling save icon

let validateObject = createProjectObject(); //used to verify if all input elements value are valid

let patternArrObj = {
	projectId: /^[A-Z]{3,}_?[A-Z]*\d*_?[A-Z]*\d$/i,
	ownerName: /^[A-Z][a-z]+-?[A-Z]*[a-z]*\s[A-Z][a-z]+$/,
	title: /^[A-Z][a-z]{4,}/,
	category: /[a-z]+$/,
	hours: /^\d\d{0,1}$/,
	rate: /^[0-5]$/,
	status: /[a-z]+$/,
};

/**
 * link event to add button, sort button, reset button.
 * link event to write to local storage button, clear button, load button and append button.
 * link event to input element from inputHandler() method.
 * disable add button and disable write to local storage button.
 * author: Vinh Dat Hoang & Christopher Hu
 */
function handlers() {
	let buttonAdd = document.getElementById('btnAdd');
	buttonAdd.addEventListener('click', updateProjectsTable);

	document.getElementById('sortBtn').addEventListener('click', sortProjects);
	document.getElementById('filter').addEventListener('keyup', filterProjects);
	
	let buttonReset = document.getElementById('btnReset');
	buttonReset.addEventListener('click', btnReset);
	
	let div = document.getElementsByClassName('bigDivForm');
	inputHandler(div);
	let name = ['btnAdd','btnWrite'];
	for(let i = 0; i < 2 ; i++){
		
		let button = document.getElementById(name[i]);
		disable_Button(button);
	}

	let buttonWriteLocal = document.getElementById('btnWrite');
	buttonWriteLocal.addEventListener('click', saveAllProjects2Storage);

	let buttonClear = document.getElementById('btnClear');
	buttonClear.addEventListener('click', confirmDeleteStorage);

	let buttonLoad = document.getElementById('btnLoad');
	buttonLoad.addEventListener('click', confirmLoadingStorage);

	let buttonAppend = document.getElementById('btnAppend');
	buttonAppend.addEventListener('click', appendAllProjects2Storage);
}

/**
 * confirmDelete is used as an event listener, attached to the delete icons located in every row in the table.
 * Prompts the user to confirm for deletion, invokes removeRow if true.
 * author: Christopher Hu
 * @param {HTMLCollection} event as 'click'
 * @returns 
 */
 let confirmDelete = (event) => confirm('Are you sure you want to DELETE this project?') ? removeRow(event) : alert('Okay.');

 /**
  * confirmDeleteStorage is used as an event listner, attached to the clear local storage button.
  * prompts the user to confirm clear local storage.
  * author: Vinh Dat Hoang
  * @param {HTMLCollection} event 
  * @returns --> call clearAllProjectFromStorage() function if true and alert message if false
  */
 let confirmDeleteStorage = (event) => confirm('Are you sure you want to CLEAR All Local Storage?') ? clearAllProjectFromStorage(event) : alert('Okay.');
 
 /**
  * confirmLoadingStorage is used as an event listner, attached to the load local storage button.
  * prompts the user to confirm clear all projects in table and load all projects from local storage button.
  * author: Vinh Dat Hoang
  * @param {HTMLCollection} event 
  * @returns --> call readAllProjectsFromStorage() function if true and alert message if false
  */
 let confirmLoadingStorage = (event) => confirm('Are you sure you want to DELETE All ROW and LOAD All project in local Storage?') ? readAllProjectsFromStorage(event) : alert('Okay.');