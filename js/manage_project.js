'use strict';

/**
 * createProjectObject
 * Creates and returns an empty object containing
 * fields to store data relating to projects.
 * @returns a skeleton object.
 * author: Christopher Hu
 */
function createProjectObject() {
	return {
				projectId: null,
				ownerName: null,
				title: null,
				category: null,
				status: null,
				hours: null,
				rate: null,
				description: null
			};

}



/**
 * retrieving data from input element and store them in an array object called arrayObj.
 * call function createRowFromArrayObjects() to create table rows from data stored inside of arrayObj.
 * call function enable_disable_WriteLocalButton() to verify if table is empty.
 * author: Vinh Dat Hoang and Christopher Hu
 */
function updateProjectsTable() {
	let dataInput = createProjectObject();
	dataInput.projectId = document.getElementById('projectId').value;
	dataInput.ownerName = document.getElementById('ownerName').value;
	dataInput.title = document.getElementById('title').value
	dataInput.category = document.getElementById('category').value
	dataInput.hours = document.getElementById('hours').value
	dataInput.rate = document.getElementById('rate').value;
	dataInput.status = document.getElementById('status').value;
	dataInput.description = document.getElementById('shortDescription').value;
	arrayObj.push(dataInput);

	createRowFromArrayObjects(dataInput);

	let button = document.getElementById('btnWrite');
	enable_disable_WriteLocalButton(button);

	let statusbar = document.getElementById('filterResultText');
	statusbar.innerText = `A project have been added to the table...`;

	btnReset();	
}

/**
 * reset input element fields to null.
 * disable add button.
 * validateObject is set to false.
 * hide icon and span 
 * author: Vinh Dat Hoang
 */
function btnReset() {
	document.getElementById('projectId').value = null;
	document.getElementById('ownerName').value = null;
	document.getElementById('title').value = null;
	document.getElementById('category').value = null;
	document.getElementById('hours').value = null;
	document.getElementById('rate').value = null;
	document.getElementById('status').value = null;
	document.getElementById('shortDescription').value = null;

	let button = document.getElementById('btnAdd');
	disable_Button(button);

	for (let key in validateObject) {
		validateObject[key] = false;
	}

	let feedbackText = document.getElementsByClassName('feedbackText');
	for (let textBox of feedbackText) {
		textBox.setAttribute('style', 'display:none');
	}

	let feedbackIcon = document.getElementsByClassName('feedbackIcon');
	for (let IconBox of feedbackIcon) {
		IconBox.setAttribute('style', 'display:none');
	}
}

/**
 * toggle between edit and save function.
 * Swicth icon image between save and edit.
 * author: Vinh Dat Hoang
 * @param {HTMLCollection} event --> input element for edit/saved icon
 */
function editSaveHandler(event) {

	let enableEdit = 0;
	let elem = document.getElementById(event.target.id).parentNode.parentNode;
	let td = elem.getElementsByTagName('td');
	
	if (enableSave == 1) {
		saveInput(td, elem);
		this.setAttribute('src', '../images/Edit_icon.png');
		enableEdit = 1;
		enableSave = 0;
	}
	if (this.src.indexOf("Edit_icon.png" >= 0) && enableEdit == 0) {
		editInput(td);
		this.setAttribute('src', '../images/save_icon.png');
		enableSave = 1;
	}
}


/**
 * create input elements to allow user to edit.
 * author: Vinh Dat Hoang (coding) & Christopher Hu (provided advice)
 * @param {HTMLCollection} td 
 */
function editInput(td) {
	
	let tdIndex = 1;

	while (tdIndex < td.length - 1) {
		let input = document.createElement('input');
		input.setAttribute('type', 'text');
		input.setAttribute('value', `${td[tdIndex].innerText}`); // copy value from row fields to input element value
		td[tdIndex].innerText = "";
		td[tdIndex].appendChild(input);
		tdIndex++;
	}
}

/**
 * transfer new value entered by user to arrayObj and table row fields.  
 * author: Vinh Dat Hoang (coding) & Christopher Hu (provided advice)
 * @param {HTMLCollection} td 
 * @param {HTMLCollection} elem 
 */
function saveInput(td, elem) {

	let tdIndex = 1;

	let inputObject = createProjectObject();

	for (let i = 1; i < td.length - 1; i++) {
		let input = td[i].getElementsByTagName('input')[0]; // copy new value from input elements to table row fields
		td[i].innerText = input.value;
	}

	inputObject.projectId = td[0].innerText;
	inputObject.ownerName = td[1].innerText;
	inputObject.title = td[2].innerText;
	inputObject.category = td[3].innerText;
	inputObject.status = td[4].innerText;
	inputObject.hours = td[5].innerText;
	inputObject.rate = td[6].innerText;
	inputObject.description = td[7].innerText;

	// set new data to arrayObj
	let tr = document.getElementsByTagName('tr');
	for (let i = 1; i < tr.length; i++) {
		if (elem == tr[i]) {
			arrayObj[i - 1] = inputObject;
		}
	}

	// update status bar
	let project = td[0].innerText;
	let statusbar = document.getElementById('filterResultText');
	statusbar.innerText = `${project} have been saved...`;
}

/**
 * sortProjects function will sort the projects array (known as arrayObj)
 * according to which key the user has selected.
 * After sorting is completed, the project table will be
 * reconstructed.
 * author: Christopher Hu
 */
function sortProjects() {
	let sortBy = document.getElementById('sort').value;
	if (sortBy == 'hours' || sortBy == 'rate') {
		arrayObj.sort(sortProjectByNumber);
	}
	else {
		arrayObj.sort(sortProjectsByString);
	}

	reconstructProjectTable(arrayObj);
}

/**
 * transfer all projects inside of the table to local storage.
 * author: Vinh Dat Hoang
 */
function saveAllProjects2Storage() {

	// clear old saved projects inside of local storage
	localStorage.clear();

	// save projects to local storage
	let jsonString = JSON.stringify(arrayObj);
	let length = localStorage.length
	localStorage.setItem(`project${length + 1}`, `${jsonString}`);

	// update status bar
	let totalProject = arrayObj.length;
	let statusbar = document.getElementById('filterResultText');
	statusbar.innerText = `${totalProject} projects have been saved to local storage...`;
}

/**
 * append new projects from the table to local storage.
 * author: Vinh Dat Hoang
 */
function appendAllProjects2Storage() {

	let jsonString = JSON.stringify(arrayObj);
	let length = localStorage.length
	localStorage.setItem(`project${length + 1}`, `${jsonString}`);

	// update status bar
	let totalProject = arrayObj.length;
	let statusbar = document.getElementById('filterResultText');
	statusbar.innerText = `${totalProject} projects have been append to local storage...`;
}

/**
 * clear local storage
 * author: Vinh Dat Hoang
 */
function clearAllProjectFromStorage() {
	localStorage.clear();

	// update status bar
	let statusbar = document.getElementById('filterResultText');
	statusbar.innerText = 'all projects have been removed from local storage ...';
}

/**
 * load from local storage.
 * author: Vinh Dat Hoang
 */
function readAllProjectsFromStorage() {
	arrayObj = [];

	let length = localStorage.length
	for (let i = 0; i < length; i++) {
		let key = localStorage.key(i);
		let local = localStorage.getItem(`${key}`);
		let rowElement = JSON.parse(`${local}`);
		for (let j = 0; j < rowElement.length; j++) {
			arrayObj.push(rowElement[j]);
		}
	}

	reconstructProjectTable(arrayObj);

	// update status bar
	let totalProject = arrayObj.length;
	let statusbar = document.getElementById('filterResultText');
	statusbar.innerText = `${totalProject} projects have been loaded from local storage...`;
}




/**
 * takes the query from the user and filters the existing projects array
 * into another variable (sortedObjectArray).
 * Filtered projects will be displayed onscreen.
 * author: Christopher Hu
 */
function filterProjects() {
	let filterString = document.getElementById('filter').value;
	sortedObjectArray = arrayObj.reduce(function(prev,elem) {
		for(let key in elem){
			if(elem[key] === filterString && filterString != ""){
				prev.push(elem);
			}
		}
		return prev;
	}, []);
	
	if (sortedObjectArray.length > 0){
		reconstructProjectTable(sortedObjectArray);
	}
	else{
		reconstructProjectTable(arrayObj);
	}

	let statusbar = document.getElementById('filterResultText');
	let filterLength = sortedObjectArray.length;
	statusbar.innerText = `Query has found ${filterLength} project(s).`;
}